# quiz

quiz is a java project make for Juan Carrasco to approve test in 3it company
## Installation

Clone and run in eclipse software
```bash
mvn clean install
```
## Usage
cd target
java -jar /quiz/src/main/java/com/it/quiz/QuizApplication.java

# REST API

The REST API to the quiz app is described below.
## Get list of quiz

### Request

`GET /quizzes/`

    curl -i -H 'Accept: application/json' http://localhost:8080/quizzes

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: 2
    [{
        "id": 1,
        "email": "juanandrescarrasco91@gmail.com",
        "gender": "ROCK"
    }]

## Create a new Quiz

### Request

`POST /quiz/`

    curl -i -H 'Accept: application/json' -d 'email=juanandrescarrasco91@gmail.com&gender=POP' http://localhost:8080/quiz

### Response

    HTTP/1.1 201 Created
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json
    Location: /thing/1
    Content-Length: 36

    OK


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)