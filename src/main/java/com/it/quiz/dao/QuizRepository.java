package com.it.quiz.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.it.quiz.model.Quiz;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {

}
