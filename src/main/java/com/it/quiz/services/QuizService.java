package com.it.quiz.services;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;
import org.springframework.stereotype.Service;

import com.it.quiz.dao.QuizRepository;
import com.it.quiz.model.Quiz;

@Service
public class QuizService implements QuizRepository {

	@Autowired
	private QuizRepository repository;

	@Override
	public List<Quiz> findAll() {
		return repository.findAll();
	}

	@Override
	public List<Quiz> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public List<Quiz> findAllById(Iterable<Integer> ids) {
		return repository.findAllById(ids);
	}

	@Override
	public <S extends Quiz> List<S> saveAll(Iterable<S> entities) {
		return repository.saveAll(entities);
	}

	@Override
	public void flush() {
		repository.flush();

	}

	@Override
	public <S extends Quiz> S saveAndFlush(S entity) {
		try {
			return repository.saveAndFlush(entity);
		} catch (DataIntegrityViolationException e) {
			throw (e);
		}
	}

	@Override
	public <S extends Quiz> List<S> saveAllAndFlush(Iterable<S> entities) {
		return repository.saveAllAndFlush(entities);
	}

	@Override
	public void deleteAllInBatch(Iterable<Quiz> entities) {
		repository.deleteAllInBatch(entities);
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Integer> ids) {
		repository.deleteAllByIdInBatch(ids);

	}

	@Override
	public void deleteAllInBatch() {
		repository.deleteAllInBatch();
	}

	@Override
	public Quiz getOne(Integer id) {
		return repository.getOne(id);
	}

	@Override
	public Quiz getById(Integer id) {
		return repository.getById(id);
	}

	@Override
	public <S extends Quiz> List<S> findAll(Example<S> example) {
		return repository.findAll(example);

	}

	@Override
	public <S extends Quiz> List<S> findAll(Example<S> example, Sort sort) {
		return repository.findAll(example, sort);

	}

	@Override
	public Page<Quiz> findAll(Pageable pageable) {
		return repository.findAll(pageable);

	}

	@Override
	public <S extends Quiz> S save(S entity) {
		try {
			return repository.save(entity);
		} catch (DataIntegrityViolationException e) {
			throw (e);
		}

	}

	@Override
	public Optional<Quiz> findById(Integer id) {
		return repository.findById(id);

	}

	@Override
	public boolean existsById(Integer id) {
		return repository.existsById(id);

	}

	@Override
	public long count() {
		return repository.count();

	}

	@Override
	public void deleteById(Integer id) {
		repository.deleteById(id);

	}

	@Override
	public void delete(Quiz entity) {
		repository.delete(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Integer> ids) {
		repository.deleteAllById(ids);

	}

	@Override
	public void deleteAll(Iterable<? extends Quiz> entities) {
		repository.deleteAll(entities);

	}

	@Override
	public void deleteAll() {
		repository.deleteAll();

	}

	@Override
	public <S extends Quiz> Optional<S> findOne(Example<S> example) {
		return repository.findOne(example);
	}

	@Override
	public <S extends Quiz> Page<S> findAll(Example<S> example, Pageable pageable) {
		return repository.findAll(example, pageable);
	}

	@Override
	public <S extends Quiz> long count(Example<S> example) {
		return repository.count(example);
	}

	@Override
	public <S extends Quiz> boolean exists(Example<S> example) {
		return repository.exists(example);
	}

	@Override
	public <S extends Quiz, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) {
		return repository.findBy(example, queryFunction);
	}

}
