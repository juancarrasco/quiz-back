package com.it.quiz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.it.quiz.dao.QuizRepository;
import com.it.quiz.model.Quiz;
import com.it.quiz.services.QuizService;

@RestController
public class QuizController {
	@Autowired
	private QuizService service;

	@PostMapping("/quiz")
	public ResponseEntity<Object> saveQuiz(@RequestBody Quiz quiz)throws Exception {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(service.save(quiz));
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getSuppressed() );
		}
	}

	@GetMapping("/quizzes")
	public ResponseEntity<List<Quiz>> getAll() {
		return ResponseEntity.ok(service.findAll());
	}

}
