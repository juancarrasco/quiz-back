package com.it.quiz.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.transaction.Transactional;

import com.it.quiz.dao.QuizRepository;
import com.it.quiz.enums.Genders;
import com.it.quiz.model.Quiz;

@ExtendWith(MockitoExtension.class)
@Transactional
@SpringBootTest
public class QuizServiceTests {

	@Mock
	private QuizRepository repository;

	@InjectMocks
	private QuizService service;

	private Quiz quiz;

	String email = "jcarrasco@acid.cl";

	@BeforeEach
	public void setup() {
		// employeeRepository = Mockito.mock(EmployeeRepository.class);
		// employeeService = new EmployeeServiceImpl(employeeRepository);
		quiz = new Quiz();
		quiz.setId(1);
		quiz.setEmail(email);
		quiz.setGender(Genders.POP);
	}

	@DisplayName("JUnit test for saveQuiz method")
	@Test
	public void givenQuizObject_whenSaveEmployee_thenReturnQuizObject() {
		// Arrange
		lenient().when(repository.findById(quiz.getId())).thenReturn(Optional.empty());

		lenient().when(repository.save(quiz)).thenReturn(quiz);

		System.out.println(repository);
		System.out.println(service);

		// Active
		Quiz savedEmployee = service.save(quiz);

		System.out.println(savedEmployee.getId());

		// Assert
		assertThat(savedEmployee).isNotNull();
	}

	// JUnit test for saveEmployee method
	@DisplayName("JUnit test for saveQuiz method which throws exception")
	@Test
	public void givenExistingEmail_whenSaveEmployee_thenThrowsException() {
		// Arrange
		lenient().when(service.findById(quiz.getId())).thenReturn(Optional.of(quiz));

		System.out.println("repository ->" + repository);
		System.out.println("service ->" + service);
		System.out.println("quiz ->" + quiz.getId());

		// Active
		org.junit.jupiter.api.Assertions.assertNull(service.saveAndFlush(quiz));

		// then
		verify(repository, Mockito.never()).save(null);
	}
	
	@DisplayName("JUnit test for existsById method which throws exception")
    @Test
    public void givenExistsById() {
		lenient().when(service.existsById(quiz.getId())).thenReturn(true);
    }
}
