package com.it.quiz.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import com.it.quiz.dao.QuizRepository;
import com.it.quiz.enums.Genders;
import com.it.quiz.model.Quiz;

@SpringBootTest
@Transactional
public class QuizTests {
	@Resource
	private QuizRepository quizRepository;

	@Test
	public void givenQuiz_whenSave_thenGetOk() {
		/** arrange */
		String email = "jcarrasco@acid.cl";
		Quiz quiz = new Quiz();
		quiz.setId(1);
		quiz.setEmail(email);
		quiz.setGender(Genders.ROCK);

		/** act */
		quizRepository.save(quiz);
		Optional<Quiz> quizPersist = quizRepository.findById(1);

		/** assert */
		assertEquals(email, quizPersist.get().getEmail());
	}

	@Test
    public void errorQuiz_whenSave_sameEmail() {
    	/** arrange */
    	String email = "jcarrasco@acid.cl";
        Quiz quiz = new Quiz();
        quiz.setId(1);
        quiz.setEmail(email);
        quiz.setGender(Genders.ROCK);
        
        /** act */
        quizRepository.saveAndFlush(quiz);
                
        /** assert */
        assertThrows(DataIntegrityViolationException.class, () -> quizRepository.saveAndFlush(quiz));
	}
}
