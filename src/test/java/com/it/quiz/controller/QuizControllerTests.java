package com.it.quiz.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;

import com.it.quiz.enums.Genders;
import com.it.quiz.model.Quiz;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class QuizControllerTests {
	@Autowired
	private QuizController controller;
	@Autowired
	private TestRestTemplate restTemplate;
	@LocalServerPort
	private int port;
	private Quiz quiz;
	String email = "jcarrasco@acid.cl";

	@BeforeEach
	public void setup() {
		// employeeRepository = Mockito.mock(EmployeeRepository.class);
		// employeeService = new EmployeeServiceImpl(employeeRepository);
		quiz = new Quiz();
		quiz.setId(1);
		quiz.setEmail(email);
		quiz.setGender(Genders.POP);
	}
	
	@Test
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}
	
	@Test
	public void quizShouldReturnDefaultMessage() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/quizzes",
				List.class)).asList();
	}
	
	@Test
	public void quizShouldSaveOk() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
 
        HttpEntity<Quiz> request = new HttpEntity<>(quiz, headers);
         
        ResponseEntity<String> result = this.restTemplate.postForEntity("http://localhost:" + port + "/quiz", request, String.class);
        //Verify request succeed
       assertEquals(200, result.getStatusCodeValue());
	}
	@Test
	public void quizShouldNotSaveOk() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
 
        HttpEntity<Quiz> request = new HttpEntity<>(quiz, headers);
         
        ResponseEntity<String> result = this.restTemplate.postForEntity("http://localhost:" + port + "/quiz", request, String.class);
        //Verify request succeed
       assertEquals(200, result.getStatusCodeValue());
	}
}
